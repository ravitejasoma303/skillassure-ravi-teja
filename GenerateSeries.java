package lab1;
import java.util.*;
public class GenerateSeries {
	static void series1(int N)
	{
		int sum=1,sign=1;
		for(int i=1;i<=N;i++)
		{
			System.out.print(sum*sign+",");
			sign=sign * -1;
			sum=sum+(i*i);
		}
		System.out.println();
	}
	static void series2(int N)
	{
		for(int i=1;i<=N;i++)
		{
			if (i%2==0)
			System.out.print(i*i+",");
		}
		System.out.println();
	}
	static void series3(int N)
	{
		int n1=0,n2=1,sum=0;
		System.out.print(n2+",");
		for(int i=2;i<=N;i++)
		{
			sum=n1+n2;
			System.out.print(sum+",");
			n1=n2;
			n2=sum;
		}
		System.out.println();
	}
	static void series4(int N)
	{
		int n1=1,n2=4,n3=7;
		int sum=0;
		System.out.print(n1+","+n2+","+n3+",");
		for(int i=1;i<=N;i++)
		{
			sum=n1+n2+n3;
			System.out.print(sum+",");
			n1=n2;
			n2=n3;
			n3=sum;
		}
		System.out.println();
	}
	static void series5(int N)
	{
		int x=1,y=-2;
		System.out.print(x+","+y+",");
		for(int i=1;i<=N;i++)
		{
			x+=3;
			y-=4;
			System.out.print(x+","+y+",");

		}
		System.out.println();
	}
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number:");
		int N=sc.nextInt();
		sc.close();
		series1(N);
		series2(N);
		series3(N);
		series4(N);
		series5(N);
	
	}

}
